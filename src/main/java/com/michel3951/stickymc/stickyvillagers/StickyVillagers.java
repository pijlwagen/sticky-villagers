package com.michel3951.stickymc.stickyvillagers;

import com.michel3951.stickymc.stickyvillagers.Database.Sql;
import com.michel3951.stickymc.stickyvillagers.Events.OnVillagerTrade;
import com.michel3951.stickymc.stickyvillagers.Support.Chat;
import com.michel3951.stickymc.stickyvillagers.Support.Logger;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public final class StickyVillagers extends JavaPlugin {

    @Override
    public void onEnable() {
        Logger.info(Chat.colored("&aStickyVillagers has been enabled"));

        File f = new File(this.getDataFolder() + "/");
        if (!f.exists()) f.mkdir();


        Sql.createDatabase();

        this.getServer().getPluginManager().registerEvents(new OnVillagerTrade(), this);
    }

    @Override
    public void onDisable() {
        Logger.info(Chat.colored("&cStickyVillagers has been disabled"));
    }
}
