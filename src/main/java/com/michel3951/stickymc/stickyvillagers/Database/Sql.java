
package com.michel3951.stickymc.stickyvillagers.Database;

import com.michel3951.stickymc.stickyvillagers.Support.Logger;

import java.sql.*;

public class Sql {

    public static void createDatabase() {
        String url = "jdbc:sqlite:./plugins/StickyVillagers/history.db";

        try (Connection conn = DriverManager.getConnection(url)) {
            if (conn != null) {
                conn.close();
                createTables();
            }
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        }

    }

    private static void createTables() {
        Connection conn = getConnection();

        if (conn == null) {
            return;
        }

        try {
            String query = "CREATE TABLE IF NOT EXISTS trades (uuid TEXT, enchantment TEXT, most_recent_trade TIMESTAMP DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (uuid, enchantment) )";
            Statement stmt = conn.createStatement();

            stmt.execute(query);
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        }

        try {
            String query = "CREATE TABLE IF NOT EXISTS trade_history (name TEXT, enchantment TEXT, traded_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP); CREATE INDEX IF NOT EXISTS `trade_find` ON `trade_history` (name, enchantment)";
            Statement stmt = conn.createStatement();

            stmt.execute(query);
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        }
    }

    public static Connection getConnection() {
        Connection conn = null;

        try {
            // db parameters
            String url = "jdbc:sqlite:./plugins/StickyVillagers/history.db";
            // create a connection to the database
            conn = DriverManager.getConnection(url);

            return conn;
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        }

        return null;
    }
}
