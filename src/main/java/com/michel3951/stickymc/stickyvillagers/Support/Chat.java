package com.michel3951.stickymc.stickyvillagers.Support;

import org.bukkit.ChatColor;

public class Chat {

    public static String colored(String message) {
        return ChatColor.translateAlternateColorCodes('&', message);
    }

}
