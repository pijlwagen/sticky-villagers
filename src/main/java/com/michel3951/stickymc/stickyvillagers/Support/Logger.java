package com.michel3951.stickymc.stickyvillagers.Support;

import org.bukkit.Bukkit;

public class Logger {

    public static void info(String message) {
        System.out.println("[StickyVillagers] " + message);
    }

    public static void error(String message) {
        Bukkit.getConsoleSender().sendMessage(Chat.colored("[StickyVillagers] " + message));
    }
}
